##
# Copyright 2009-2023 Ghent University
#
# This file is part of EasyBuild,
# originally created by the HPC team of Ghent University (http://ugent.be/hpc/en),
# with support of Ghent University (http://ugent.be/hpc),
# the Flemish Supercomputer Centre (VSC) (https://www.vscentrum.be),
# Flemish Research Foundation (FWO) (http://www.fwo.be/en)
# and the Department of Economy, Science and Innovation (EWI) (http://www.ewi-vlaanderen.be/en).
#
# https://github.com/easybuilders/easybuild
#
# EasyBuild is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation v2.
#
# EasyBuild is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with EasyBuild.  If not, see <http://www.gnu.org/licenses/>.
##
"""
EasyBuild support for installing software using 'conda' with a separate 'bin' folder
containing symlinks to the principal software files, implemented as an easyblock.

@author: Duncan Mortimer (University of Oxford)
"""

import os
import os.path
import tempfile


from easybuild.easyblocks.generic.binary import Binary
from easybuild.easyblocks.generic.conda import Conda
from easybuild.framework.easyconfig import CUSTOM
from easybuild.tools.run import run_cmd
from easybuild.tools.modules import get_software_root
from easybuild.tools.build_log import EasyBuildError


class CondaWrapped(Conda):
    """Support for installing software using 'conda', creating a separate 'bin' folder
    that will be added to the path, so avoiding poluting the user's path with conda tools."""

    @staticmethod
    def extra_options(extra_vars=None):
        """Extra easyconfig parameters specific to Conda easyblock."""
        extra_vars = Conda.extra_options(extra_vars)
        extra_vars.update({
            'bin_list': [None, "List of binaries/scripts installed by Conda to symlink", CUSTOM],
            'bin_dir': ['usrbin', "Sub-folder to use for symlinks - will be added to PATH", CUSTOM],
        })
        return extra_vars

    def extract_step(self):
        """Copy sources via extract_step of parent, if any are specified."""
        if self.src:
            super(CondaWrapped, self).extract_step()

    def install_step(self):
        """Install software using 'conda env create' or 'conda create' &
        'conda install'"""
        super(CondaWrapped, self).install_step()
        bin_dir = os.path.join(self.installdir, self.cfg['bin_dir'])
        if self.cfg['bin_list']:
            os.mkdir(bin_dir)
            for b in self.cfg['bin_list']:
                target = os.path.join(self.installdir, b)
                dest = os.path.join(bin_dir, os.path.basename(b))
                os.symlink(target, dest)

    def make_module_extra(self):
        """Add the install directory to the PATH."""
        txt = super(Binary, self).make_module_extra()
        self.log.debug("make_module_extra added this: %s", txt)
        return txt

    def make_module_req_guess(self):
        """
        A dictionary of possible directories to look for.
        """
        return {
            'PATH': [self.cfg['bin_dir']],
            'MANPATH': ['man', os.path.join('share', 'man')],
        }
